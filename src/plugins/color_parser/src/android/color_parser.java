package cashid.color_parser;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.graphics.Palette;

/**
 * This class parses a matching text and background from an image file.
 */
public class color_parser extends CordovaPlugin
{
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException
	{
		if(action.equals("getColorStyleFromImageFile"))
		{
			String message = args.getString(0);
			this.getColorStyleFromImageFile(message, callbackContext);
			return true;
		}
		return false;
	}

	private void getColorStyleFromImageFile(String filename, CallbackContext callbackContext)
	{
		if(filename != null && filename.length() > 0)
		{
			// Assume default colors (black text, light muted grey background)
			int text_color = 0;
			int background_color = ((256^2 * 224) + (256^1 * 224) + (256^0 * 224));

			// Create a bitmap from the image file.
			Bitmap file_bitmap = BitmapFactory.decodeFile(filename);

			// Create a palette from the image bitmap.
			Palette file_palette = Palette.from(file_bitmap).generate();

			// Attempt to create a vibrant palette swatch from the palette.
			Palette.Swatch vibrant_swatch = file_palette.getVibrantSwatch();

			// If the vibrant palette swatch was successfully extracted..
			if(vibrant_swatch != null)
			{
				// Replace the default colors with the ones from the palette swatch.
				text_color = vibrant_swatch.getBodyTextColor();
				background_color = vibrant_swatch.getRgb();
			}

			// Return the selected colors as a CSS style text.
			callbackContext.success(String.format("{ \"color\": \"#%06X\", \"background-color\": \"#%06X\" }", (0xFFFFFF & text_color), (0xFFFFFF & background_color)));
		}
		else
		{
			callbackContext.error("Expected one non-empty string argument.");
		}
	}
}
